import { Button } from "react-bootstrap";
export default function Contact() {
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>Contact Us</h1>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "RobotoCondensed"
        }}
      >
        <form>
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder="Name.."
          ></input>
          <br />
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder="Email.."
          ></input>
          <br />
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder="Subject.."
          ></input>
          <br />
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder="Your Message.."
          ></input>
          <br />
          <Button style={{ marginBottom: "10px" }}>Send email</Button>
        </form>
      </div>
    </div>
  );
}
