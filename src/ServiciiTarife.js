import {
  Container,
  Row,
  Col,
  Carousel,
  ListGroup,
  Card,
  Button
} from "react-bootstrap";
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
export default function ServiciiTarife() {
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>Services</h1>
      <div>
        <Container fluid>
          <Col className="carousel-content">
            <Carousel>
              <Carousel.Item>
                <img
                  style={{ height: "400px", width: "100vh" }}
                  src={require("/img/Pediatric dentistry.jpg")}
                  alt="First slide"
                />
                <Carousel.Caption>
                  <h3 style={{ fontFamily: "Comfortaa", color: "black" }}>
                    Pediatric Dentistry
                  </h3>
                  <Link to="/contact" className="btn btn-primary">
                    Contact
                  </Link>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  style={{ height: "400px", width: "100vh" }}
                  src={require("/img/General care.jpg")}
                  alt="Second slide"
                />

                <Carousel.Caption>
                  <h3 style={{ fontFamily: "Comfortaa", color: "black" }}>
                    General care
                  </h3>
                  <Link to="/contact" className="btn btn-primary">
                    Contact
                  </Link>
                </Carousel.Caption>
              </Carousel.Item>
              <Carousel.Item>
                <img
                  style={{ height: "400px", width: "100vh" }}
                  src={require("/img/Additional treatment.jpg")}
                  alt="Third slide"
                />

                <Carousel.Caption>
                  <h3 style={{ fontFamily: "Comfortaa", color: "black" }}>
                    Additional treatments
                  </h3>
                  <Link to="/contact" className="btn btn-primary">
                    Contact
                  </Link>
                </Carousel.Caption>
              </Carousel.Item>
            </Carousel>
          </Col>
        </Container>
      </div>
    </div>
  );
}
