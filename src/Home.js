import "./styles.css";
import { Link } from "react-router-dom";
export default function Acasa() {
  return (
    <div style={{ height: "100vh" }}>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>
        Welcome to DentaWeb
      </h1>
      <div>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            color: "#2D2D3A",
            padding: "10px"
          }}
        >
          We're glad you decided to become our customer for resolving your
          dental health concerns. We'll do our best to deliver the best possible
          healthcare and customer service to you!
        </p>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            padding: "10px"
          }}
        >
          Our main long-term goal is always achieving complex results for your
          dental health.
        </p>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            padding: "10px"
          }}
        >
          But in the process, we also keep the focus on giving you the best
          customer service. We're always making our dental office as safe place
          as possible!
        </p>
        <p
          style={{
            fontFamily: "RobotoCondensed",
            fontSize: "20px",
            padding: "10px"
          }}
        >
          That is taken into account via our numerous pain management options,
          which aim to prevent any discomfort or dental care fears our patients
          might have.
        </p>
        <div>
          <Link
            to="/ServiciiTarife"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            Services
          </Link>
          <Link
            to="/DespreNoi"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            About Us
          </Link>
          <Link
            to="/Medici"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            Doctors
          </Link>
          <Link
            to="/contact"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            Contact
          </Link>
          <Link
            to="/Noutati"
            style={{ padding: "10px", margin: "10px" }}
            className="btn btn-primary"
          >
            News
          </Link>
        </div>
      </div>
    </div>
  );
}
