import { Link } from "react-router-dom";
export default function Medici() {
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>Doctors</h1>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        Dr. Hikaru Nakamura
      </div>
      <img
        alt="doctor"
        src={require("/img/Dr man.png")}
        style={{ height: "200px", margin: "10px" }}
      ></img>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        Dr. Hikaru Nakamura specialisez in orthodontics and dentofacial
        orthopedics; pediatric dentistry; periodontics; prosthodontics;
      </p>
      <Link to="/contact" className="btn btn-primary">
        Contact
      </Link>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          marginTop: "15px",
          fontFamily: "Comfortaa"
        }}
      >
        Dr. Anna Cramling
      </div>
      <img
        alt="doctor"
        src={require("/img/Dr woman.png")}
        style={{ height: "200px", margin: "10px" }}
      ></img>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        Dr. Anna Cramling specialisez in oral and maxillofacial surgery; oral
        and maxillofacial pathology; endodontics; public health dentistry; and
        oral and maxillofacial radiology.
      </p>
      <Link to="/contact" className="btn btn-primary">
        Contact
      </Link>
    </div>
  );
}
