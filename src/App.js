import "./styles.css";
import { Route, Switch, Link, BrowserRouter as Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./Home";
import Contact from "./Contact";
import DespreNoi from "./DespreNoi";
import Medici from "./Medici";
import Noutati from "./Noutati";
import ServiciiTarife from "./ServiciiTarife";

export default function App() {
  return (
    <div className="App">
      <header>
        <Router>
          <div className="nav-bar">
            <ul>
              <li className="nav-bar-item" id="nav-bar-logo">
                <img src={require("/img/dentaWeb.png")} alt="joe rogan"></img>
              </li>
              <li className="nav-bar-item" id="nav-bar-home">
                <Link to="/">Home</Link>
              </li>
              <li className="nav-bar-item">
                <Link to="/Noutati">News</Link>
              </li>
              <li className="nav-bar-item">
                <Link to="/Contact">Contact</Link>
              </li>
              <li className="nav-bar-item">
                <Link to="/Medici">Doctors</Link>
              </li>
              <li className="nav-bar-item">
                <Link to="/DespreNoi">About Us</Link>
              </li>
              <li className="nav-bar-item">
                <Link to="/ServiciiTarife">Services</Link>
              </li>
            </ul>
          </div>
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route exact path="/Noutati">
              <Noutati />
            </Route>
            <Route exact path="/Contact">
              <Contact />
            </Route>
            <Route exact path="/DespreNoi">
              <DespreNoi />
            </Route>
            <Route exact path="/Medici">
              <Medici />
            </Route>
            <Route exact path="/ServiciiTarife">
              <ServiciiTarife />
            </Route>
          </Switch>
        </Router>
      </header>
    </div>
  );
}
