export default function DespreNoi() {
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>About Us</h1>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        Appointment
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        We allow for an easier booking option than anybody else. Thousands of
        dental services were booked with us already!
      </p>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        Expertise
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        Our team comprises of highly skilled and knowledgeable professionals. We
        all have a common goal - helping you!
      </p>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        Access
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        We offer same-day appointments - 6 days a week.
      </p>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        Best price guarantee
      </div>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        The pricing for our dental services are always updated online and
        reflect the afofrdability of our high quality expertise!
      </p>

      <iframe
        title="COVID presentation"
        style={{ marginTop: "30px", marginRight: "200px", marginLeft: "200px" }}
        height="400"
        width="50%"
        src="https://www.youtube.com/embed/uK2OE48V8X8"
      ></iframe>
    </div>
  );
}
